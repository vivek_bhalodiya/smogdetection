package com.example.vivekbhalodiya.smogdetector;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CoordinatesCallback{
  public static boolean isDataLoaded =false;
  List<Fragment> fragments = new ArrayList<>();
  private static final int MULTIPLE_PERMISSIONS = 455;
  String[] permissions = new String[] {
          Manifest.permission.ACCESS_COARSE_LOCATION,
          Manifest.permission.ACCESS_FINE_LOCATION};
  PageViewAdapter pageViewAdapter;
  private boolean isNetworkAvailable = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    toolbar.setTitle("");
    initiateFragments();

    }

  private boolean checkPermissions() {
    int result;
    List<String> listPermissionsNeeded = new ArrayList<>();
    for (String p : permissions) {
      result = ContextCompat.checkSelfPermission(this, p);
      if (result != PackageManager.PERMISSION_GRANTED) {
        listPermissionsNeeded.add(p);
      }
    }
    if (!listPermissionsNeeded.isEmpty()) {
      ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
      return false;
    }
    return true;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
    switch (requestCode) {
      case MULTIPLE_PERMISSIONS: {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // permissions granted.
        } else {
          String permission = "";
          for (String per : permissions) {
            permission += "\n" + per;
          }
          Toast.makeText(this,"Permissions not granted",Toast.LENGTH_SHORT).show();
        }
        return;
      }
    }
  }

  private void initiateFragments() {
    ViewPager viewPager = findViewById(R.id.view_pager);
    TabLayout tabLayout = findViewById(R.id.tab_layout);
    fragments.add(new SensorDataFragment());
    fragments.add(new MapFragment());
    pageViewAdapter = new PageViewAdapter(getSupportFragmentManager(), fragments);
    viewPager.setAdapter(pageViewAdapter);
    tabLayout.setupWithViewPager(viewPager);
  }


  @Override protected void onStart() {
    super.onStart();
    checkPermissions();
    if(!checkNetworkState()) {
      Toast.makeText(getApplicationContext(), "No Internet Connection.", Toast.LENGTH_SHORT).show();
      isNetworkAvailable = false;
    }
    else
      isNetworkAvailable = true;
  }

  @Override
  protected void onResume() {
    super.onResume();
    if(!checkNetworkState()) {
      Toast.makeText(getApplicationContext(), "No Internet Connection.", Toast.LENGTH_SHORT).show();
      isNetworkAvailable = false;
    }
    else
      isNetworkAvailable = true;
  }

  @Override
  public void onCoordinatesRecieved(List<CoordinatesModel> coordinatesModels) {
    pageViewAdapter.setMarkers(coordinatesModels);
  }

  private boolean checkNetworkState() {
    final ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
    return activeNetwork != null && activeNetwork.isConnected();
  }
}
