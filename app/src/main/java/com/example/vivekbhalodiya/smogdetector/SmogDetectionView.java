package com.example.vivekbhalodiya.smogdetector;

import com.example.vivekbhalodiya.smogdetector.api.model.ThinkSpeakResponse;

import java.util.List;

/**
 * Created by Vivek on 24-03-2018.
 */

public interface SmogDetectionView {
    void loadGraphData(String created_at, String updated_at, List<FeedsModel> listOfValue);
    void setProgressBar(boolean status);
    void setCoordinates(List<CoordinatesModel> coordinates);
    void showToast();
}
