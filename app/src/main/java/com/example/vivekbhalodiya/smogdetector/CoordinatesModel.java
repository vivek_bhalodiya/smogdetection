package com.example.vivekbhalodiya.smogdetector;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Vivek on 25-03-2018.
 */

public class CoordinatesModel {
    LatLng latLng;
    String createdAt;

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
