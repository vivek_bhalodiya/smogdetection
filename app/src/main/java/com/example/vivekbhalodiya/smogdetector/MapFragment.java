package com.example.vivekbhalodiya.smogdetector;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    MapView mMapView;
    GoogleMap googleMap;
    CoordinatesCallback coordinatesCallback;
    List<CoordinatesModel> latLngList = new ArrayList<>();
    boolean isCoordinatesReady = false;
    FloatingActionButton refreshFab;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        refreshFab = view.findViewById(R.id.refresh_fab);

        mMapView = (MapView) view.findViewById(R.id.mapView);

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(checkNetworkState())
            mMapView.getMapAsync(this);
        else
            Toast.makeText(getContext(),"No Internet Connection.",Toast.LENGTH_SHORT).show();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(),"No Location Permission", Toast.LENGTH_SHORT).show();
        }
        else
            googleMap.setMyLocationEnabled(true);

        if(checkNetworkState()) {
            refreshFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isCoordinatesReady && !latLngList.isEmpty()) {
                        for (CoordinatesModel latLng : latLngList) {
                            if (latLng != null) {
                                googleMap.addMarker(new MarkerOptions().position(latLng.getLatLng()).title("Location").snippet(getFormattedDate(latLng.getCreatedAt())));
                            }
                        }
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLngList.get(0).getLatLng()).zoom(12).build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        Toast.makeText(getContext(), "Data Refreshed", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Data not ready please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else
            Toast.makeText(getContext(),"No Internet Connection.",Toast.LENGTH_SHORT).show();

    }

    private String getFormattedDate(String createdAt) {
        String date="";
        SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat normalFormat = new SimpleDateFormat("dd MMM HH:mm");
        try {
            date = normalFormat.format(mFormat.parse(createdAt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    void getMarkers(List<CoordinatesModel> coordinatesModels){
        int i=0;
        CoordinatesModel modal=new CoordinatesModel();
        if(!coordinatesModels.isEmpty()){
            while(i < coordinatesModels.size()-1){
                if(coordinatesModels.get(i).getLatLng().latitude !=
                coordinatesModels.get(i+1).getLatLng().latitude &&
                        coordinatesModels.get(i).getLatLng().longitude !=
                                coordinatesModels.get(i+1).getLatLng().longitude)
                    modal= new CoordinatesModel();
                    modal.setLatLng(coordinatesModels.get(i).getLatLng());
                    modal.setCreatedAt(coordinatesModels.get(i).getCreatedAt());
                    latLngList.add(modal);
                i++;
            }
            Log.d("MapFragment New Lat ", String.valueOf(latLngList.size()));
            isCoordinatesReady = true;
        }
        else{
            Log.d("MapFragment","Coords are null");
        }

    }

    private boolean checkNetworkState() {
        final ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
}