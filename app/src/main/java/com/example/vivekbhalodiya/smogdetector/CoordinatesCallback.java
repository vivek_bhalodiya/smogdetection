package com.example.vivekbhalodiya.smogdetector;

import java.util.List;

/**
 * Created by Vivek on 25-03-2018.
 */

public interface CoordinatesCallback {
    void onCoordinatesRecieved(List<CoordinatesModel> coordinatesModels);
}
