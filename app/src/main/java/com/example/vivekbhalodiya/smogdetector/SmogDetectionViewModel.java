package com.example.vivekbhalodiya.smogdetector;

import android.util.Log;

import com.example.vivekbhalodiya.smogdetector.api.model.Feed;
import com.example.vivekbhalodiya.smogdetector.api.model.ThinkSpeakResponse;
import com.example.vivekbhalodiya.smogdetector.api.services.ThinkSpeakInterface;
import com.example.vivekbhalodiya.smogdetector.api.services.ThinkSpeakSpeakApi;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vivek on 24-03-2018.
 */

class SmogDetectionViewModel {
    private ThinkSpeakInterface thinkspeakApi = new ThinkSpeakSpeakApi().getClient().create(ThinkSpeakInterface.class);
    private SmogDetectionView fragmentView;
    private FeedsModel feedsModel = new FeedsModel();
    private List<FeedsModel> temperature = new ArrayList<>();
    private List<FeedsModel> humidity = new ArrayList<>();
    private List<FeedsModel> dustIndex = new ArrayList<>();
    private List<FeedsModel> smokeSmog = new ArrayList<>();
    private List<FeedsModel> greenHouseGas = new ArrayList<>();
    private List<FeedsModel> latitutelist = new ArrayList<>();
    private List<FeedsModel> longitute = new ArrayList<>();
    private List<FeedsModel> myLatList;
    private List<CoordinatesModel> coordModel = new ArrayList<>();
    private String created_at;
    private String updated_at;

    void getThinkSpeakData(){
        fragmentView.setProgressBar(true);
        thinkspeakApi
                .getThinkSpeakData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ThinkSpeakResponse>() {
                    @Override
                    public void onNext(ThinkSpeakResponse thinkSpeakResponseResponse) {
                     Log.d("TAG", String.valueOf(thinkSpeakResponseResponse.getFeeds().size()));
                     created_at=thinkSpeakResponseResponse.getChannel().getCreatedAt();
                     updated_at = thinkSpeakResponseResponse.getChannel().getUpdatedAt();
                     setAllFeedsData(thinkSpeakResponseResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        fragmentView.setProgressBar(false);
                    }

                    @Override
                    public void onComplete() {
                        fragmentView.setProgressBar(false);
                    }
                });
    }

    private void setAllFeedsData(ThinkSpeakResponse thinkSpeakResponseResponse) {
        int i=0;
        myLatList = new ArrayList<>();
        for(Feed feed : thinkSpeakResponseResponse.getFeeds()){
            FeedsModel model = new FeedsModel();
            model.setValue(feed.getField7());
            model.setCreated_at(feed.getCreatedAt());
            myLatList.add(model);
        }


        for (Feed feed :thinkSpeakResponseResponse.getFeeds()){
            Log.d("Tag Values",feed.getField1());
            feedsModel.created_at = feed.getCreatedAt();
            feedsModel.value = feed.getField1();
            temperature.add(feedsModel);

            feedsModel = new FeedsModel();
            feedsModel.created_at = feed.getCreatedAt();
            feedsModel.value = feed.getField2();
            humidity.add(feedsModel);


            feedsModel = new FeedsModel();
            feedsModel.created_at = feed.getCreatedAt();
            feedsModel.value = feed.getField3();
            dustIndex.add(feedsModel);


            feedsModel = new FeedsModel();
            feedsModel.created_at = feed.getCreatedAt();
            feedsModel.value = feed.getField4();
            smokeSmog.add(feedsModel);


            feedsModel = new FeedsModel();
            feedsModel.created_at = feed.getCreatedAt();
            feedsModel.value = feed.getField5();
            greenHouseGas.add(feedsModel);


            feedsModel = new FeedsModel();
            feedsModel.created_at = feed.getCreatedAt();
            feedsModel.setValue(feed.getField6());
            longitute.add(feedsModel);
            Log.d("LngValue",feedsModel.getValue());

            feedsModel = new FeedsModel();
            feedsModel.created_at = feed.getCreatedAt();
            feedsModel.setValue(feed.getField7());
            latitutelist.add(feedsModel);

            Log.d("Padma", latitutelist.get(i).getValue());
            Log.d("PadmaField",feed.getField7());
            i++;
        }
        if(!latitutelist.isEmpty() && !longitute.isEmpty())
             processLongLat();
    }

    private void processLongLat() {
        Log.d("InsideLongLat","Yo");
        int i =0;
        while (i < latitutelist.size()){
            if(!(latitutelist.get(i).value.equals("nan") || latitutelist.get(i).value.equals("0"))
            && !(longitute.get(i).value.equals("nan") || longitute.get(i).value.equals("0")))
            {
                CoordinatesModel model = new CoordinatesModel();
                float lat;
                float longit;
                try {

                    Log.d("Kuntal", myLatList.get(i).getValue());
                    lat= Float.parseFloat(myLatList.get(i).getValue());
                    long longLat = Integer.parseInt(myLatList.get(i).getValue());
                    Log.d("RitvikLat", String.valueOf(lat));
                    Log.d("RitvikLong", String.valueOf(longLat));
                    lat = lat / 1000000;
                     longit= Float.parseFloat(longitute.get(i).getValue());
                    longit = longit / 1000000;
                    model.setLatLng(new LatLng(lat,longit));
                    model.setCreatedAt(latitutelist.get(i).getCreated_at());
                    coordModel.add(model);
                }
                catch (NumberFormatException e){
                    Log.e("Hello","hello",e);
                }

            }
            i++;
        }
        fragmentView.setCoordinates(coordModel);
        fragmentView.showToast();
    }

    void setCallback(SmogDetectionView fragment){
        this.fragmentView = fragment;
    }

    void loadSpecificFeedData(int position){
        switch (position){
            case 1 : fragmentView.loadGraphData(created_at,updated_at,temperature);
                    break;
            case 2 : fragmentView.loadGraphData(created_at,updated_at,humidity);
                break;
            case 3 : fragmentView.loadGraphData(created_at,updated_at,dustIndex);
                break;
            case 4 : fragmentView.loadGraphData(created_at,updated_at,smokeSmog);
                break;
            case 5 : fragmentView.loadGraphData(created_at,updated_at,greenHouseGas);
                break;
                default: fragmentView.loadGraphData("","",new ArrayList<FeedsModel>());
        }
    }
}
