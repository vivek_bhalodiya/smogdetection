package com.example.vivekbhalodiya.smogdetector;

/**
 * Created by Vivek on 25-03-2018.
 */

public class FeedsModel {
    String created_at = "";
    String value = "";

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
