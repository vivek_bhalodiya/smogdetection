package com.example.vivekbhalodiya.smogdetector.api.services

import com.example.vivekbhalodiya.smogdetector.api.model.ThinkSpeakResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

/**
 * Created by Vivek on 24-03-2018.
 */
interface ThinkSpeakInterface {
    @GET("/channels/453360/feeds.json?api_key=L89GW10JR4MPEG55&results=100")
    fun getThinkSpeakData() : Observable<ThinkSpeakResponse>
}