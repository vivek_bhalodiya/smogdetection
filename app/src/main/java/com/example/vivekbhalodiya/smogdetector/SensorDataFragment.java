package com.example.vivekbhalodiya.smogdetector;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.ParseException;
import org.angmarch.views.NiceSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.example.vivekbhalodiya.smogdetector.MainActivity.isDataLoaded;


/**
 * A simple {@link Fragment} subclass.
 */
public class SensorDataFragment extends Fragment implements OnChartGestureListener
        , OnChartValueSelectedListener, SmogDetectionView {

    ProgressDialog progressDialog;
    SmogDetectionViewModel smogDetectionViewModel = new SmogDetectionViewModel();
    private LineChart mChart;
    ArrayList<Entry> graphValues = new ArrayList<Entry>();
    CoordinatesCallback coordinatesCallback;
    TextView selectedGraph ;
    XAxis xAxis;
    List<String> listOfDates = new ArrayList<>();
    private boolean isNetworkAvailable =false;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        coordinatesCallback = (CoordinatesCallback) context;
    }

    public SensorDataFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!checkNetworkState()) {
            Toast.makeText(getContext(), "No Internet Connection.", Toast.LENGTH_SHORT).show();
            isNetworkAvailable = false;
        }
        else
            isNetworkAvailable = true;
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)  {
        View view = inflater.inflate(R.layout.fragment_temperature, container, false);

        selectedGraph = view.findViewById(R.id.selected_graph_textview);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading data...");
        progressDialog.setCancelable(false);
        smogDetectionViewModel.setCallback(this);
        mChart = (LineChart) view.findViewById(R.id.chart1);
        setupGraphView();

        if(checkNetworkState()) {
            if (!isDataLoaded) {
                smogDetectionViewModel.getThinkSpeakData();
                isDataLoaded = true;
            }
        }
        else
            Toast.makeText(getContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.my_option, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.temperature_item:
                if(checkNetworkState()) {
                    selectedGraph.setText(AppConstants.TEMPERATURE);
                    smogDetectionViewModel.loadSpecificFeedData(1);
                }
                else
                    Toast.makeText(getContext(),"Check Internet Connection.",Toast.LENGTH_SHORT).show();
                break;
            case R.id.humidity_item:
                if(checkNetworkState()){
                selectedGraph.setText(AppConstants.HUMIDITY);
                smogDetectionViewModel.loadSpecificFeedData(2);
                }
                else
                    Toast.makeText(getContext(),"Check Internet Connection.",Toast.LENGTH_SHORT).show();
                break;
            case R.id.dust_index_item:
                if(checkNetworkState()) {
                    selectedGraph.setText(AppConstants.DUST_INDEX);
                    smogDetectionViewModel.loadSpecificFeedData(3);
                }
                else
                    Toast.makeText(getContext(),"Check Internet Connection.",Toast.LENGTH_SHORT).show();
                break;
            case R.id.smoke_smog_item:
                if(checkNetworkState()) {
                    selectedGraph.setText(AppConstants.SMOG_SMOKE);
                    smogDetectionViewModel.loadSpecificFeedData(4);
                }
                else
                    Toast.makeText(getContext(),"Check Internet Connection.",Toast.LENGTH_SHORT).show();
                break;
            case R.id.green_house_gas_item:
                if(checkNetworkState()) {
                    selectedGraph.setText(AppConstants.GREEN_HOUSE_GAS);
                    smogDetectionViewModel.loadSpecificFeedData(5);
                }
                else
                    Toast.makeText(getContext(),"Check Internet Connection.",Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

        return false;
    }


    private void setupGraphView() {
        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);
        mChart.setDrawGridBackground(false);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        // mChart.setScaleXEnabled(true);
        // mChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        // mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(getContext(), R.layout.custom_marker_view);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart

        // x-axis limit line
        LimitLine llXAxis = new LimitLine(10f, "Index 10");
        llXAxis.setLineWidth(4f);
        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);

         xAxis = mChart.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setPosition(XAxis.XAxisPosition.TOP_INSIDE);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(true);
        xAxis.setCenterAxisLabels(true);
        xAxis.setGranularity(1f); // one hour
        //xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line


        /*//LimitLine ll1 = new LimitLine(150f, "Upper Limit");
        ll1.setLineWidth(4f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        ll1.setTextSize(10f);
        */
       LimitLine ll2 = new LimitLine(-0f, "Zero");
        ll2.setLineWidth(4f);
        ll2.enableDashedLine(10f, 10f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        //leftAxis.addLimitLine(ll1);
        leftAxis.addLimitLine(ll2);
        leftAxis.setAxisMaximum(220);
        leftAxis.setAxisMinimum(-20f);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        mChart.getAxisRight().setEnabled(false);

        //mChart.getViewPortHandler().setMaximumScaleY(2f);
        //mChart.getViewPortHandler().setMaximumScaleX(2f);





    }

    void setData(int count, float range) {

        /*ong now = TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis());


        float from = now;

        // count = hours
        float to = now + count;

        // increment by 1 hour
        for (float x = from; x < to; x++) {

            float y = getRandom(range, 50);
            graphValues.add(new Entry(x, y)); // add one entry per hour
        }*/
        mChart.invalidate();
        mChart.animateX(2500);
        //mChart.invalidate();


        LineDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet)mChart.getData().getDataSetByIndex(0);
            set1.setValues(graphValues);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(graphValues, "DataSet 1");

            set1.setDrawIcons(false);

            // set the line to be drawn like this "- - - - - -"
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            //set1.setLabel("Realm LineDataSet");
            set1.setDrawCircleHole(true);
            set1.setColor(ColorTemplate.rgb("#FF5722"));
            set1.setCircleColor(ColorTemplate.rgb("#FF5722"));
            set1.setLineWidth(1.8f);
            set1.setCircleRadius(1f);
            set1.setValueTextSize(0f);

            //set1.enableDashedLine(10f, 5f, 0f);
            //set1.enableDashedHighlightLine(10f, 5f, 0f);
            //set1.setColor(Color.BLACK);
            //set1.setCircleColor(Color.BLACK);
            //set1.setLineWidth(1f);
            //set1.setCircleRadius(3f);
            //set1.setDrawCircleHole(false);
            //set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            //set1.setFormLineWidth(1f);
            //set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            //set1.setFormSize(15.f);


            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.fade_red);
            set1.setFillDrawable(drawable);

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            mChart.setData(data);

            // get the legend (only possible after setting data)
            Legend l = mChart.getLegend();

            // modify the legend ...
            l.setForm(Legend.LegendForm.LINE);

        }
    }

    protected float getRandom(float range, float startsfrom) {
        return (float) (Math.random() * range) + startsfrom;
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }


    @Override
    public void loadGraphData(String created_at, String updated_at, List<FeedsModel> listOfValue) {
        Log.d("sensorclass","abcd");
        int i=0;
        graphValues = new ArrayList<>();
        for(FeedsModel model : listOfValue){
            if(!model.getValue().equals("nan")) {
                graphValues.add(new Entry(i, Float.parseFloat(model.getValue())));
                i++;
            }
        }
        listOfDates.clear();
        for (FeedsModel feedsModel : listOfValue)
            listOfDates.add(feedsModel.getCreated_at());
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            private SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            private SimpleDateFormat normalFormat = new SimpleDateFormat("dd MMM HH:mm");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

              /*  long millis = TimeUnit.HOURS.toMillis((long) value);
                return mFormat.format(new Date(millis));*/
                String stringValue="";
                Date dt;
                if (listOfDates.size() >= 0 && value >= 0) {
                    if (value < listOfDates.size()) {
                        try {
                            dt = mFormat.parse(listOfDates.get((int) value));
                            stringValue = normalFormat.format(dt);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        stringValue = "";
                    }
                }else {
                    stringValue = "";
                }
                return stringValue;
            }
        });
        setData(100,50);
    }

    @Override
    public void setProgressBar(boolean status) {
        if(status)
            progressDialog.show();
        else
            progressDialog.dismiss();
    }

    @Override
    public void setCoordinates(List<CoordinatesModel> coordinates) {
        coordinatesCallback.onCoordinatesRecieved(coordinates);
    }

    @Override
    public void showToast() {
        Toast.makeText(getContext(),"Data loaded successfully.",Toast.LENGTH_SHORT).show();
    }

    private boolean checkNetworkState() {
        final ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
}