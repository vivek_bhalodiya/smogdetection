package com.example.vivekbhalodiya.smogdetector;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.List;

/**
 * Created by Vivek on 25-03-2018.
 */

public class PageViewAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    public PageViewAdapter(FragmentManager supportFragmentManager, List<Fragment> fragments) {
        super(supportFragmentManager);
        this.fragments = fragments;
    }

    @Override public int getCount() {
        return fragments.size();
    }

    @Override public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Nullable
    @Override public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Live Data";
        } else {
            return "Map";
        }
    }

    public void setMarkers(List<CoordinatesModel> coordinatesModels) {
        MapFragment mapFragment = (MapFragment) fragments.get(1);
        if(mapFragment!= null){
            mapFragment.getMarkers(coordinatesModels);
        }
        else{
            Log.d("MapFragment","is null");
        }
    }
}
