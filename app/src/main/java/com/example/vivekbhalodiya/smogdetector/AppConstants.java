package com.example.vivekbhalodiya.smogdetector;

/**
 * Created by Vivek on 24-03-2018.
 */

public class AppConstants {
    public final static String BASE_URL = "https://api.thingspeak.com/";
    final static String TEMPERATURE = "Temperature";
    final static String HUMIDITY = "Humidity";
    final static String DUST_INDEX = "Dust Index(ug/m3)";
    final static String SMOG_SMOKE =  "Smoke/Smog(ppbv)";
    final static String GREEN_HOUSE_GAS = "Green House Gas";
}
